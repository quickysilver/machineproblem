//Write a function that accepts a list as a parameter.
//Conditions:
//The list contains string values of the colors of the rainbow. (roygbiv).
//Some of the colors can have duplicates in the list.
//Count the occurrence of each color, and return a list of mapped values.
//If the value of the string, is not in the colors of the rainbow, it will be counted under "Others"
//The case of the string does not matter, meaning it should be able to process both uppercase, lowercase, and mix case.
//Ex:
//Sample Input: listOf("red", "blue", "blue", "pink")
//Expected Output: { red = 1, blue = 2, others = 1}

fun main () {
    val rainbowColor = mutableListOf<String>("red", "blue", "blue", "pink")
    countColors(rainbowColor)
}

fun countColors(rainbowColor: List<String>) {
    val colorVal =  rainbowColor.map {
        if(it.lowercase() == "red" || it.lowercase() == "orange"  || it.lowercase() == "yellow" ||
            it.lowercase() == "green"  || it.lowercase() == "blue"  || it.lowercase() == "indigo" ||
            it.lowercase() == "violet") it to 0
        else "others" to 0 }.toMap().toMutableMap()

    rainbowColor.map{ color2 ->
        when(color2.lowercase()){
            "red" -> colorVal.put("red", colorVal.getValue("red").plus(1))
            "orange" -> colorVal.put("orange", colorVal.getValue("orange").plus(1))
            "yellow" -> colorVal.put("yellow", colorVal.getValue("yellow").plus(1))
            "green" -> colorVal.put("green", colorVal.getValue("green").plus(1))
            "blue" -> colorVal.put("blue", colorVal.getValue("blue").plus(1))
            "indigo" -> colorVal.put("indigo", colorVal.getValue("indigo").plus(1))
            "violet" -> colorVal.put("violet", colorVal.getValue("violet").plus(1))
            else ->  colorVal.put("others", colorVal.getValue("others").plus(1))
        }
    }
    println(colorVal.filter { it -> it.value > 0 })
}
