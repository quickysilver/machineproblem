//Write an application that allows a user to enter the names and birthdates of up to
// 10 friends. Continue to prompt the user for names and birthdates until the user
// enters the sentinel value ZZZ for a name or has entered 10 names, whichever comes
// first. When the user is finished entering names, produce a count of how many names
// were entered, and then display the names. In a loop, continuously ask the user to type
// one of the names and display the corresponding birthdate or an error message if the
// name has not been previously entered. The loop continues until the user enters ZZZ for a name.

fun main () {
    val users = 10
    var ctr = 0
    val quit = "ZZZ"
    var birthDate: String? = null
    var celebrant: String? = null
    val arrBirthDate = arrayOfNulls<String>(users)
    val arrCelebrant = arrayOfNulls<String>(users)
    println("Input celebrant's name or input \"ZZZ\" to quit")
    celebrant = readLine()!!
    while (celebrant != quit && ctr < 10) {
        println("Input celebrant's birthdate (dd/mm/yyyy)")
        birthDate = readLine()!!
        arrCelebrant[ctr] = celebrant
        arrBirthDate[ctr] = birthDate
        println(
            "Input celebrant's name or input \"ZZZ\" to quit"
        )
        celebrant = readLine()!!
        ctr++
    }
    println("Number of celebrant inputted: $ctr")
    println("The inputted celebrants are:")
    for (i in 0 until ctr) {
        println(arrCelebrant[i])
    }
    var x = true
    var nameContain = false
    while (x) {
        println(
            "Input celebrant's name or input \"ZZZ\" to quit."
        )
        celebrant = readLine()!!
        if ((celebrant == quit)) x = false else {
            var i = 0
            while (i < ctr && !nameContain) {
                if ((arrCelebrant[i] == celebrant)) {
                    nameContain = true
                    birthDate = arrBirthDate[i]
                }
                i++
            }
            if (nameContain) {
                println(
                    ("$celebrant's birth date is  $birthDate")
                )
            } else {
                println(
                    ("$celebrant's birth date is not yet entered.")
                )
            }
        }
        nameContain = false
    }
}