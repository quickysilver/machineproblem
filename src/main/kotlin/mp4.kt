//Write an application that contains an array of 10 multiple-choice quiz
// questions related to your favorite hobby. Each question contains three
// answer choices. Also create an array that holds the correct answer to each
// question—A, B, or C. Display each question and verify that the user enters
// only A, B, or C as the answer—if not, keep prompting the user until a valid
// response is entered. If the user responds to a question correctly, display
// Correct!; otherwise, display The correct answer is ... and the letter of the
// correct answer. After the user answers all the questions, display the number of
// correct and incorrect answers
fun main() {
    val hobbies = arrayOf (
        "questions" to "Who is my favorite international singer?",
        "answers" to mapOf (
            "A" to "Eminem",
            "B" to "Ed Sheeran",
            "C" to "Shawn Mendes"
        ),
        "questions" to "What is my most cooked dish?",
        "answers" to mapOf (
            "A" to "Tinola",
            "B" to "Adobo",
            "C" to "Sinigang"
        ),
        "questions" to "What is my favorite movie?",
        "answers" to mapOf (
            "A" to "Home",
            "B" to "50 First Dates",
            "C" to "Click"
        ),
        "questions" to "What is my most favorite series?",
        "answers" to mapOf (
            "A" to "Grey's Anatomy",
            "B" to "Big Bang Theory",
            "C" to "Flash"
        ),
        "questions" to "What is my favorite anime?",
        "answers" to mapOf (
            "A" to "Detective Conan",
            "B" to "Naruto",
            "C" to "One Piece"
        ),
        "questions" to "Who is my favorite anime character?",
        "answers" to mapOf (
            "A" to "Luffy",
            "B" to "Zoro",
            "C" to "Naruto"
        ),
        "questions" to "What is my favorite online video game?",
        "answers" to mapOf (
            "A" to "League of Legends",
            "B" to "Valorant",
            "C" to "MIR4"
        ),
        "questions" to "What is my favorite color?",
        "answers" to mapOf (
            "A" to "Blue",
            "B" to "Sage",
            "C" to "Black"
        ),
        "questions" to "What is my favorite baked goods?",
        "answers" to mapOf (
            "A" to "Cookies",
            "B" to "Brownies",
            "C" to "Cakes"
        ),
        "questions" to "What is my favorite drink?",
        "answers" to mapOf (
            "A" to "Softdrinks",
            "B" to "Milktea",
            "C" to "Water"
        ),
    )

    val answersToHobbies = arrayListOf (
        "A",
        "B",
        "B",
        "A",
        "C",
        "B",
        "C",
        "C",
        "A",
        "C")

    var score = 0
    var counter = 0

    for(i in answersToHobbies) {
        println(hobbies[counter].second)
        println(hobbies[counter+1].second)
        var quit = 0
        while (quit != 1){
            val userInput = readLine()!!
            quit = if (userInput == "A" ||  userInput == "B"  || userInput == "C") {
                if (userInput == i) {
                    println("Correct! \n")
                    score++
                } else println("The correct answer is: $i \n")
                1
            } else 0
        }
        counter += 2
    }
    println("Your score is: $score")
}