//A personal phone directory contains room for first names and phone numbers for
// 30 people. Assign names and phone numbers for the first 10 people. Prompt the
// user for a name, and if the name is found in the list, display the corresponding
// phone number. If the name is not found in the list, prompt the user for a phone
// number, and add the new name and phone number to the list. Continue to prompt the
// user for names until the user enters quit. After the lists are full (containing 30 names),
// do not allow the user to add new entries.

fun main(){
    var quit = false
    var personalPhoneDirectory: MutableMap<String, String> = mutableMapOf(
        "Kurt" to "09711711711",
        "Daniel" to "09876543211",
        "Antonio" to "09111111111",
        "Shelly" to "09222222222",
        "Camille" to "09333333333",
        "Ancheta" to "09444444444",
        "Miguel" to "09555555555",
        "Vito" to "09666666666",
        "Kyle" to "09777777777",
        "Kirsten" to "09888888888",
    )

    while(!quit){
        if(personalPhoneDirectory.size <= 30) {
            println("Input Name: ")
            var userName = readLine()!!
            if (userName.lowercase() != "quit") personalphonedirectory(personalPhoneDirectory, userName)
            else quit = true
        }else println("Contacts reached limit.")
    }
}

fun personalphonedirectory(personalPhoneDirectory: MutableMap<String,String>, phoneName: String){
    if (personalPhoneDirectory.containsKey(phoneName))
        println("Number: ${personalPhoneDirectory.getValue(phoneName)}")
    else {
        println("Input the number:")
        var newNum = readLine()!!
        personalPhoneDirectory[phoneName] = newNum
        println("$phoneName - $newNum got added to phone directory. ")
    }
}