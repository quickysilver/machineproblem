//Write a function that accepts a string as a parameter.
//Conditions:
//It can only accept up to 15 characters.
//If the number of characters is even, return the string in reverse.
//If the number of characters is odd, return the string in alphabetical order.

fun main () {
    println(char("OnePiece"))
}

fun char (characters: String){

        if(characters.length > 15){
            println("It can only accept up to 15 characters")
        }
        else if(characters.length % 2 == 0){
            println("The string is even. ${characters.reversed()}")
        }
        else if(characters.length % 2 != 0){
            println("The string is odd. ${characters.toSortedSet()}")
        }
}
